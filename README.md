# Readme for the simulation environment for the Experts in Teams masters project 2020.

This repository should be cloned to the computer.

Download the PX4 firmware repo version and select version v1.11.1
    
    cd docker-workspace
    git clone https://github.com/PX4/Firmware.git
    cd Firmware
    git checkout tags/v1.11.1
    git submodule update --init --recursive

It can be benificial to add your user to the docker group such that sudo is not needed to run docker commands.

    sudo groupadd docker
    sudo usermod -aG docker $USER

and reboot the computer.

Build the Docker image in the root directory of this git repo:
    
    docker build --tag eit_px4_gazebo .

In main terminal run:
    
    ./run-docker-image.sh

If other terminals are needed run the following to open a bash shell:

    docker exec -it eit_px4_gazebo-tester bash

Now build the PX4 firmware inside the Docker image according to the guide.

    cd /workspace/Firmware
    DONT_RUN=1 make px4_sitl_default gazebo    

To use the SDU drones and environments these have to be linked to the Firmware directory.

    cd /workspace
    ./ln_airframes.sh

Build the ROS workspace in the Docker image:

    cd /workspace/ros-ws
    catkin build

Now test the system to see if it works.
In one terminal run:
    
    beit-ws
    roslaunch mavros px4.launch fcu_url:="udp://:14540@127.0.0.1:14557"

And in another run:

    beit-ws
    roslaunch eit_playground posix.launch vehicle:=sdu_drone

If your machine has a discrete nvidia graphics card gazebo might give an error related to GLX
To fix this install nvidia-docker from this link:
	https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker

And run the docker image with:
	./run-docker-image-gpu.sh

* Run the simulation environment
    - Teminal 1:
    ```
        ./run-docker-image.sh or ./run-docker-image-gpu.sh
        beit-ws
        roslaunch eit_playground uav_launch.launch world:=fence_world vehicle:=sdu_drone_depth_cam gui:=false
    ```
    - Terminal 2:
    ```
       docker exec -it eit_px4_gazebo-tester bash
       beit-ws
       rosrun eit_playground simpleDrone.py
    ```
    - Get the image from the uav:
    ```
       docker exec -it eit_px4_gazebo-tester bash
       beit-ws
       rosrun rqt_image_view rqt_image_view 
    ```
    - Start the navigation controller:
    ```
        docker exec -it eit_px4_gazebo-tester bash
        beit-ws
        roslaunch navigation_controller launch_navigationController.launch 
    ```

## Mavros waypoint messages
* https://mavlink.io/en/messages/common.html
* Frame:
    * 2: Accept Radius -> Acceptance radius (if the sphere with this radius is hit, the waypoint counts as reached)
    * 6: Longitude	Longitude
* Command:
    * 530 : MAV_CMD_SET_CAMERA_MODE
    * 22  : MAV_CMD_NAV_TAKEOFF
    * 16  : MAV_CMD_NAV_WAYPOINT
    * 178 : MAV_CMD_DO_CHANGE_SPEED 
    * 20  : MAV_CMD_NAV_RETURN_TO_LAUNCH

## Useful commands

* Connect with the raspberry pi:
```
username : ubuntu
password : uas4ever
```
* Launch mavros on companion computer
```roslaunch mavros px4.launch fcu_url:="/dev/PX4:921600"```
* Copy files to raspberry
``` scp -r simple_navigation/ ubuntu@192.168.43.6:/home/ubuntu/catkin_ws/src```
* Export ros master from the PI : ```export ROS_IP=192.168.43.6```
* Get ros master from the PC : ```Export ROS_MASTER_URI=http://192.168.43.6:11311```
* 43 43
in_pos:
  - data: 55.4723626
  - data: 10.3254913
  - data: 3.0
* 43 0
in_pos:
  - data: 55.4719755
  - data: 10.3254906
  - data: 3.0
