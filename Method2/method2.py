import sys
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import argparse
import cv2
import numpy as np
from skimage import data
import tensorflow as tf
import kmeanstf as ktf
import time


initial_time = time.time()  #Initialization for timer for analysis

f = cv2.VideoCapture('/content/drive/MyDrive/ColabNotebooks/4.mp4')  # Uploading video 
c = 0

out = None
fourcc = cv2.VideoWriter_fourcc(*"MP4V")
#fourcc =  cv2.CV_FOURCC(*'DIVX')
out = cv2.VideoWriter("/content/drive/MyDrive/ColabNotebooks/output_dilated2.mp4", fourcc, 15.0, (960, 540), True) # Initialization of output video

while f.isOpened():
  
  rval, image = f.read() # reading frame from the input video
  if rval == True:

  #cv2.imwrite('first_frame.jpg', frame)
    print("Frame ", c+1)

    start_time = time.time()

    
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # Translating to RGB for easier operation

    
    height, width, depth = image.shape
    image = cv2.resize(image,(int(width/2), int(height/2))) # Resizing the input frame

 # Processing the array of pixel values such that K-means can process it
    pixels = image.reshape((-1, 3)) 
    pixels = np.float32(pixel_values)

# Declaration of number of clusters and performing K-means clustering using Tensorflow library
    k = 15
    kmeansTunnel = ktf.TunnelKMeansTF(n_clusters=k, random_state=12345)
    GPU_Test = np.random.normal(size=(500000,5))
    kmeansTunnel.fit(pixels)

#  Declaring the resulting centroids and labels and processing them
    labels = kmeansTunnel.labels_.numpy()
    centers = kmeansTunnel.cluster_centers_.numpy()

    labels = labels.flatten()
    centers = np.uint8(centers)


#  Making the image based on resulting clusters
    segmented_image = centers[labels.flatten()]
    segmented_image = segmented_image.reshape(image.shape)

# Creating new masked image
    masked_image = np.copy(image)
    masked_image = masked_image.reshape((-1, 3))


    cluster = 0
    #print(masked_image[labels == cluster])
# Flitering undesired clusters
    for cluster in range(k):
      r, g, b = centers[cluster]
      if (115 < r < 140) and (115 < g < 140) and (115 < b < 140):
        masked_image[labels == cluster] = [0, 255, 0]
      else:
        masked_image[labels == cluster] = [0, 0, 0]

    

    # masked_image = masked_image.reshape(image.shape)
    # print(masked_image.shape)

# Performing dilation of the fence segmented image
    kernel = np.ones((35,35),np.uint8)
    dilated_image = cv2.dilate(masked_image,kernel,iterations = 1)
   
    cv2.imwrite("/content/drive/MyDrive/ColabNotebooks/dilated.jpg", dilated_image)

    cont_img = cv2.imread("/content/drive/MyDrive/ColabNotebooks/dilated.jpg")
    #cont_img = cv2.imread("masked.jpg")
    cont_img = cv2.cvtColor(cont_img, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(cont_img, 127, 255, 0)

# Finding edges of the dilated image and then dilate such that only holes will be found
    cont_img = cv2.Canny(cont_img,100,200)

    kernel2 = np.ones((5,5),np.uint8)

    cont_image_dil = cv2.dilate(cont_img,kernel2,iterations = 1)
    cv2.imwrite("cont.jpg", cont_image_dil)

    cont_image_dil = cv2.imread("cont.jpg")
    cont_image_dil = cv2.cvtColor(cont_image_dil, cv2.COLOR_BGR2GRAY)
    ret, mask = cv2.threshold(cont_image_dil, 10, 255, cv2.THRESH_BINARY)
   
# Creating a mask to the original image such that holes are detected   
    mask = cv2.bitwise_not(mask)

    res = cv2.bitwise_and(image,image,mask = mask)

    
    end_time = time.time()

# Writing results on the output video
    out.write(res)
    #print(masked_image.shape)
    c = c+1

    print('Time per frame in seconds: {}'.format(end_time - start_time))
  else:
    out.release()
    f.release()
    print("Finish!")
    final_time = time.time()
    print("Total Elapsed time: {}".format(final_time - initial_time))
    print("Average time per frame: {}".format((final_time - initial_time)/c))
