#!/usr/bin/env python2
import rospy
import cv2
import sys
import numpy as np
from sensor_msgs.msg import Image, Imu, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
from geometry_msgs.msg import PoseStamped, Quaternion
from mavros_msgs.msg import AttitudeTarget
import tf
from enum import Enum
from copy import deepcopy
class mode(Enum):
    FIXED = 1
    SMOOTH = 2

def mulQ(A,B):
    Q = tf.transformations.quaternion_multiply(A,B)
    return Q/np.linalg.norm(Q)

def invQ(A):
    Q = tf.transformations.quaternion_inverse(A)
    return Q/np.linalg.norm(Q)
    

class VideoStabilizer:
    def __init__(self, imu_data, cam_info, gimbal_axis=None):
        self.Rcam = tf.transformations.euler_matrix(np.pi/2, 0, np.pi/2) #tf.transformations.euler_matrix(np.pi/2+0.5, 0, np.pi/2)  #np.asarray([[0,0,1,0],[0,1,0,0],[1,0,0,0], [0,0,0,1]]).reshape(4,4)
        #self.Rcam[0:3, 0] = -self.Rcam[0:3, 0] #rotate into left hand coordinates
        print(self.Rcam)
        self.qcam = tf.transformations.quaternion_from_matrix(self.Rcam)
        self.mav_pose = np.asarray([imu_data.orientation.x, imu_data.orientation.y, imu_data.orientation.z, imu_data.orientation.w]) 
        self.cam_pose = mulQ(self.mav_pose, self.qcam)
        self.smooth_pose = mulQ(self.mav_pose, self.qcam)
        self.imu_stamp = imu_data.header.stamp
        self.cam_stamp = imu_data.header.stamp
        self.K = np.asarray(cam_info.K).reshape(3,3)
        self.Kinv = np.linalg.inv(self.K)
        self.fix_pose = mulQ(self.mav_pose, self.qcam)
        self.gimbal_axis = gimbal_axis
        self.gimbal_set_pose = mulQ(self.mav_pose, self.qcam)
        self.shaky_pose = np.asarray([imu_data.orientation.x, imu_data.orientation.y, imu_data.orientation.z, imu_data.orientation.w]) 
        self.imu_rot = [0,0,0]
        #self.debug_pub = rospy.Publisher("stabilizer_debug", PoseStamped, queue_size=10)
        #self.debug_pub_2 = rospy.Publisher("stabilizer_debug_2", PoseStamped, queue_size=10)
        #self.debug_pub_3 = rospy.Publisher("stabilizer_debug_3", PoseStamped, queue_size=10)
    
        
        


    def stabilize(self, image):

        
               
        #imu_q = tf.transformations.quaternion_from_euler(self.imu_rot[0], self.imu_rot[1], -self.imu_rot[2])
        imu_q = tf.transformations.quaternion_from_euler( -self.imu_rot[0], self.imu_rot[1] ,self.imu_rot[2], 'rxyz')
        #imu_q = tf.transformations.quaternion_from_euler( 0, self.imu_rot[1] ,0)
        self.shaky_pose = mulQ(self.shaky_pose, imu_q)
        self.imu_rot = [0,0,0]

        #if stab_mode == mode.SMOOTH:
        
        rotq = mulQ( 
               invQ(self.cam_pose ), 
               self.smooth_pose)

        # rotq = mulQ( 
        #        invQ( mulQ(self.shaky_pose, self.qcam) ),
        #        self.smooth_pose)

        #else:
        #    rotq = mulQ( invQ(self.cam_pose), self.fix_pose)
        #    self.smooth_pose = self.fix_pose
        rotm = tf.transformations.quaternion_matrix(rotq)[0:3,0:3]
        #rotm =  rotm * self.Rcam  # rotate into camera frame
        #rotm = np.matmul(rotm, np.linalg.inv(self.Rcam[0:3,0:3]))
        euler = tf.transformations.euler_from_matrix(rotm)

            
        if self.gimbal_axis == 1:
            rotm_1 = tf.transformations.euler_matrix(0, euler[1], -euler[2])[0:3,0:3]
        else:
            rotm_1 = tf.transformations.euler_matrix(euler[0], -euler[1], -euler[2])[0:3,0:3]
        # debug = PoseStamped()
        # #debug.pose.orientation = Quaternion(rotq[0], rotq[1], rotq[2], rotq[3])
        # debug.pose.orientation = Quaternion(euler[0], euler[1], euler[2], 0)
        # debug.header.stamp = rospy.Time.now()
        # self.debug_pub.publish(debug)

        # euler = tf.transformations.euler_from_quaternion(self.cam_pose)
        # debug = PoseStamped()
        # #debug.pose.orientation = Quaternion(rotq[0], rotq[1], rotq[2], rotq[3])
        # debug.pose.orientation = Quaternion(euler[0], euler[1], euler[2], 0)
        # debug.header.stamp = rospy.Time.now()
        # self.debug_pub_2.publish(debug)
        # print(self.mav_pose)
        # print(self.cam_pose)
        # print(rotm)
        # print("rotm: ", np.shape(rotm), "k: ", np.shape(self.K))
        # print(np.shape(image))

        # euler = tf.transformations.euler_from_quaternion(self.mav_pose)
        # debug = PoseStamped()
        # #debug.pose.orientation = Quaternion(rotq[0], rotq[1], rotq[2], rotq[3])
        # debug.pose.orientation = Quaternion(euler[0], euler[1], euler[2], 0)
        # debug.header.stamp = rospy.Time.now()
        # self.debug_pub_3.publish(debug)

        homography = np.matmul( np.matmul(self.K, rotm_1), self.Kinv)#self.K * rotm * self.Kinv
        homography /= homography[2,2]
        #print(homography)
        img = cv2.warpPerspective(image, M=homography, dsize=(image.shape[1], image.shape[0]))
        img = img[50:-50,100:-100]
        #move cam pose towards mav pose for stability
        self.shaky_pose = tf.transformations.quaternion_slerp(self.shaky_pose, self.mav_pose, 0.05)
        return img


    def update_model(self, imu_data, stab_mode):
        self.mav_pose = np.asarray([imu_data.orientation.x, imu_data.orientation.y, imu_data.orientation.z, imu_data.orientation.w])
        self.cam_pose =  mulQ(self.mav_pose, self.qcam)
        dt = self.deltaT(self.imu_stamp, imu_data.header.stamp)
        #print(dt)
        angVel = np.asarray([imu_data.angular_velocity.x, imu_data.angular_velocity.y, imu_data.angular_velocity.z])
        
        self.imu_rot = self.imu_rot+ dt*angVel

        # theta = np.linalg.norm(angVel)*dt
        # axis = angVel/np.linalg.norm(angVel)
        # qUpdate = tf.transformations.quaternion_about_axis(theta, axis)

        # self.shaky_pose = mulQ(qUpdate, self.shaky_pose)


        gimbalq = None
        if self.gimbal_axis == 1:
            gimbalq = mulQ( invQ(self.shaky_pose), self.gimbal_set_pose)
            #rotm = tf.transformations.quaternion_matrix(rotq)[0:3,0:3]
            #euler = tf.transformations.euler_from_matrix(rotm)



        #alpha = 1 - np.square(np.inner(self.smooth_pose, self.mav_pose))
        #alpha = alpha * 0.4 + 0.2 # limit range
        #if True: #mode is mode.SMOOTH:
        if stab_mode == mode.FIXED:
            self.smooth_pose = tf.transformations.quaternion_slerp(self.smooth_pose, self.fix_pose, 0.3)
        else:
            smooth_mav = mulQ(self.smooth_pose, invQ(self.qcam))
            rotq = mulQ( 
               invQ( self.mav_pose), 
               smooth_mav)
            eul = tf.transformations.euler_from_quaternion(rotq)
            #print(eul)
            rotq =tf.transformations.quaternion_from_euler(0,0,-1*eul[2])    #-0.01*eul[0], 0, 0)   #eul[0], eul[1], eul[2])
            self.smooth_pose = mulQ(mulQ( smooth_mav, rotq), self.qcam)
            tmp = tf.transformations.euler_from_quaternion(self.smooth_pose)
            print("%.4f, %.4f, %.4f" % (tmp[0], tmp[1], tmp[2]) )
            #self.smooth_pose = tf.transformations.quaternion_slerp(self.smooth_pose, self.cam_pose, 0.1)
        
        #update stamp
        self.imu_stamp = imu_data.header.stamp
        return gimbalq

    def set_fixed_pose(self, pose):
        self.fix_pose = pose
        

    def deltaT(self, T1, T2):
       return T2.secs - T1.secs + 1e-9*(T2.nsecs - T1.nsecs)




class StabilizerNode:
    def __init__(self, gimbal_axis=None):
        rospy.init_node("vid_stabilizer")
        CAMERA_NAME="/mono_cam/"


        
        self.mode = mode.SMOOTH
        imu_data = rospy.wait_for_message("imu/data", Imu)
        cam_info = rospy.wait_for_message(CAMERA_NAME+"camera_info", CameraInfo)
        self.stabilizer = VideoStabilizer(imu_data, cam_info, gimbal_axis)


        self.result_vid = cv2.VideoWriter('/workspace/stabilized.avi',  
                         cv2.VideoWriter_fourcc(*'MJPG'), 
                         10, (cam_info.width-200, cam_info.height-100   ))

        self.input_vid = cv2.VideoWriter('/workspace/raw.avi',  
                         cv2.VideoWriter_fourcc(*'MJPG'), 
                         10, (cam_info.width, cam_info.height))

        self.combined_vid = cv2.VideoWriter('/workspace/sidebyside.avi',  
                         cv2.VideoWriter_fourcc(*'MJPG'), 
                         10, ((cam_info.width-200)*2, cam_info.height-100))

        self.image_pub = rospy.Publisher("stabilized_frame", Image, queue_size=10)
        self.depth_pub = rospy.Publisher("stabilized_depth", Image, queue_size=10)
        self.smooth_cam_pose_pub = rospy.Publisher("stabilized_pose", PoseStamped, queue_size=10)
        self.gimbal_pub = rospy.Publisher("Gimbal_setpoint", PoseStamped, queue_size=10)
       
        self.imu_sub = rospy.Subscriber("imu/data", Imu, self.newGyroData)
        # self.setpoint_sub = rospy.Subscriber("setpoint_raw/target_attitude", AttitudeTarget, self.newAttTarget)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber(CAMERA_NAME+"image_raw", Image, self.newFrame)
        self.depth_sub = rospy.Subscriber(CAMERA_NAME+"depth_img", Image, self.newFrame)

    def __del__(self):
        self.result_vid.release()
        self.input_vid.release()
        self.combined_vid.release()

    def newFrame(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)
        # cv2.namedWindow("Raw", cv2.WINDOW_NORMAL)
        # cv2.imshow("Raw", cv_image)
        self.input_vid.write(cv_image)
        tmp = deepcopy(cv_image)
        cv_image = self.stabilizer.stabilize(cv_image)
        # self.showImage(cv_image)
        self.result_vid.write(cv_image)
        tmp = np.append(tmp[50:-50,100:-100], cv_image, axis=1)
        #print(tmp.shape)
        self.combined_vid.write(tmp)

        try:
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
        except CvBridgeError as e:
            print(e)

    def newDepthFrame(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "32FC1")
        except CvBridgeError as e:
            print(e)

        cv_image = self.stabilizer.stabilize(cv_image)
        # self.showImage(cv_image)

        try:
            self.depth_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "32FC1"))
        except CvBridgeError as e:
            print(e)

    def newAttTarget(self, data):
        rate = np.asarray([data.body_rate.x, data.body_rate.y, data.body_rate.z])
        att = np.asarray([data.orientation.x, data.orientation.y, data.orientation.z, data.orientation.w])
        if np.linalg.norm(rate) > 0.1:
            if self.mode == mode.FIXED:
                print("Switching to:\t smooth mode")
            self.mode = mode.SMOOTH
        else:
            if self.mode == mode.SMOOTH:
                print("Swtiching to:\t fixed mode")
                cam_att = mulQ(att, self.stabilizer.qcam)
                # attm = tf.transformations.quaternion_matrix(cam_att) #[0:3,0:3]
                # roll= attm[0,0:3]
                # froll = deepcopy(roll)
                # froll[1] = 0
                # theta = np.arccos( np.inner(roll, froll) / (np.linalg.norm(roll)*np.linalg.norm(froll)))
                # print(theta)
                # rotm = tf.transformations.euler_matrix(0, 0, theta) #[0:3,0:3]
                # attm = np.matmul(rotm, attm)
                # cam_att = tf.transformations.quaternion_from_matrix(attm)
                self.stabilizer.set_fixed_pose( cam_att) #mulQ(att, self.stabilizer.qcam))
            self.mode = mode.FIXED



    def newGyroData(self, data):
        gimbalq = self.stabilizer.update_model(data, self.mode)
        stab_pose = PoseStamped()
        stab_pose.pose.orientation = Quaternion(self.stabilizer.smooth_pose[0], self.stabilizer.smooth_pose[1], self.stabilizer.smooth_pose[2], self.stabilizer.smooth_pose[3])
        #stab_pose.pose.orientation = Quaternion(self.stabilizer.shaky_pose[0], self.stabilizer.shaky_pose[1], self.stabilizer.shaky_pose[2], self.stabilizer.shaky_pose[3])
        stab_pose.header.stamp =  data.header.stamp #rospy.Time.now()
        self.smooth_cam_pose_pub.publish(stab_pose)
        if gimbalq is not None:
            gimbal_pose = PoseStamped()
            gimbal_pose.pose.orientation = Quaternion(gimbalq[0], gimbalq[1], gimbalq[2], gimbalq[3])
            gimbal_pose.header.stamp = rospy.Time.now()
            self.gimbal_pub.publish(gimbal_pose)
    

def main(args):
    node = StabilizerNode()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()


if __name__ == '__main__':
    print("Launching the stabilizer")
    
    main(sys.argv)
