#!/usr/bin/env python2

'''
    Read the waypoints sent from QGroundControl
    Add more waypoints around the corners to make the route smoother
'''
# import ROS libraries
import rospy
import mavros
from mavros_msgs.msg import Waypoint as WP
from mavros_msgs.msg import WaypointList as WPL
from mavros_msgs.srv import WaypointClear, WaypointPush
import numpy as np
from copy import deepcopy as dp

class WaypointModifier:

    def __init__(self, start_node = False):

        if start_node:
            rospy.init_node('waypoint_modifier')

        # Initialize variables
        self.Waypoints = []
        self.WaypointList = []

        # Setyp namespace
        mavros.set_namespace('mavros')

        # Setup subscribers
        # /mavros/mission/waypoints
        rospy.Subscriber('mavros/mission/waypoints', WPL, self._waypointCallback)

        # Setup publishers
        # /mavros/mission/waypoints
        self.wpl_pub = rospy.Publisher('mavros/mission/waypoints', WPL, queue_size=10)

        # Setup services
        self.wp_clear = rospy.ServiceProxy('/mavros/mission/clear', WaypointClear)
        self.wp_push = rospy.ServiceProxy('/mavros/mission/push', WaypointPush)

        # Wait until a few seconds to read the waypoints
        cnt = 0
        while cnt < 10:
            if len(self.WaypointList) > 0:
                break
            else:
                rospy.sleep(1)
            cnt += 1

    def _waypointCallback(self, data):
        ''' Read the waypoints and store them in a list ''' 
        wps = data.waypoints
        speed = 5.0
        
        # clear the previous list
        self.Waypoints = []
        self.WaypointList = []

        # Read and store the waypoints
        for i in range(len(wps)):
            self.WaypointList.append(wps[i])
            lat = wps[i].x_lat
            lon = wps[i].y_long
            alt = wps[i].z_alt
            heading = wps[i].param4
            hold = wps[i].param1
            tmp = []

            # Store the waypoints in a more helpful list
            if wps[i].command == 16 or wps[i].command == 22 or wps[i].command == 21: # waypoint or takeoff
                tmp = [lat, lon, alt, speed, heading, hold]
            elif wps[i].command == 530:
                pass
            elif wps[i].command == 178:  # change speed 
                self.Waypoints[-1][3] = wps[i].param2
                speed = wps[i].param2
            elif wps[i].command == 20:
                tmp = self.Waypoints[0]
                tmp[-1] = 0.0
            else:
                rospy.logwarn('Waypoint command not recognized' + str(wps[i].command))

            if len(tmp) > 0:
                self.Waypoints.append(tmp)

        self.WaypointList = np.array(self.WaypointList)
        self.Waypoints = np.array(self.Waypoints)

    def ClearMission(self):
        self.wp_clear()
    
    def PushMission(self, waypoints):
        self.wp_push(start_index = 0, waypoints = waypoints)

    def GetWaypoints(self):
        return self.Waypoints

    def GetWaypointList(self):
        return self.WaypointList
    
    def AddWaypoints(self):
        ''' Add more waypoints to make the path planning more accurate '''
        # Make a copy of the waypoint list for processing
        wpl = self.WaypointList.copy()
        fin_wpl = []

        cnt = 0
        if len(wpl) == 0:
            rospy.logwarn('No mission plan detected')
            return []
        
        if abs(wpl[0].x_lat) > 180:
            fin_wpl.append(wpl[cnt])
            cnt += 1

        while wpl[cnt].command != 21:
        
            # Add the current item
            fin_wpl.append(wpl[cnt])
            in_index = cnt
            x1 = wpl[cnt].x_lat
            y1 = wpl[cnt].y_long
            z = wpl[cnt].z_alt

            # Check if the speed changes on this waypoint
            if wpl[cnt + 1].command == 178:
                cnt += 1
                fin_wpl.append(wpl[cnt])
                
            # Get the next waypoint
            x2 = wpl[cnt + 1].x_lat
            y2 = wpl[cnt + 1].y_long

            # Add waypoints along the way
            wpk = dp(wpl[in_index])
            wpk.param4 = wpl[in_index + 1].param4 

            for k in [0.1, 0.25, 0.5, 0.75, 0.9]:
                x_k = x1 + k * (x2 - x1)
                y_k = y1 + k * (y2 - y1)

                if wpk.command == 22:
                    wpk.command = 16
                    wpk.param1 = 0.0

                wpk.x_lat = x_k
                wpk.y_long = y_k
                fin_wpl.append(dp(wpk))

            cnt += 1

        # Add the final waypoint - Land the drone
        fin_wpl.append(wpl[-1])

        return fin_wpl



###################################################################################################
if __name__ == "__main__":
    WPM = WaypointModifier(True)

    # Print the initial waypoints    
    #print(WPM.GetWaypoints())
    #print("------------------------")

    # Add more waypoints along the path
    fin_wpl = WPM.AddWaypoints()
    WPM.ClearMission()
    WPM.PushMission(fin_wpl)
    #rospy.sleep(5)

    # Print the new waypoints
    #print(WPM.GetWaypoints())
