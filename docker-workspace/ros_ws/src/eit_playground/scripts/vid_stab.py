#!/usr/bin/env python2
import rospy
import cv2
import sys
import numpy as np
from sensor_msgs.msg import Image, Imu, CameraInfo
from cv_bridge import CvBridge, CvBridgeError
from geometry_msgs.msg import PoseStamped, Quaternion
import tf

class VideoStabilizer:
    def __init__(self, imu_data, cam_info):
        self.Rcam = tf.transformations.euler_matrix(np.pi/2, 0, np.pi/2)  #np.asarray([[0,0,1,0],[0,1,0,0],[1,0,0,0], [0,0,0,1]]).reshape(4,4)
        #self.Rcam[0:3, 0] = -self.Rcam[0:3, 0] #rotate into left hand coordinates
        print(self.Rcam)
        self.qcam = tf.transformations.quaternion_from_matrix(self.Rcam)
        self.mav_pose = np.asarray([imu_data.orientation.x, imu_data.orientation.y, imu_data.orientation.z, imu_data.orientation.w]) 
        self.cam_pose = tf.transformations.quaternion_multiply(self.mav_pose, self.qcam)
        self.smooth_pose = self.cam_pose
        self.imu_stamp = imu_data.header.stamp
        self.cam_stamp = imu_data.header.stamp
        self.K = np.asarray(cam_info.K).reshape(3,3)
        self.Kinv = np.linalg.inv(self.K)
        self.debug_pub = rospy.Publisher("stabilizer_debug", PoseStamped, queue_size=10)
        self.debug_pub_2 = rospy.Publisher("stabilizer_debug_2", PoseStamped, queue_size=10)
        self.debug_pub_3 = rospy.Publisher("stabilizer_debug_3", PoseStamped, queue_size=10)
        
        


    def stabilize(self, image):

        rotq = tf.transformations.quaternion_multiply( tf.transformations.quaternion_inverse(self.cam_pose), self.smooth_pose)
        rotm = tf.transformations.quaternion_matrix(rotq)[0:3,0:3]
        #rotm =  rotm * self.Rcam  # rotate into camera frame
        #rotm = np.matmul(rotm, np.linalg.inv(self.Rcam[0:3,0:3]))
        euler = tf.transformations.euler_from_matrix(rotm)
        rotm = tf.transformations.euler_matrix(euler[0], euler[1], -euler[2])[0:3,0:3]
        debug = PoseStamped()
        #debug.pose.orientation = Quaternion(rotq[0], rotq[1], rotq[2], rotq[3])
        debug.pose.orientation = Quaternion(euler[0], euler[1], euler[2], 0)
        debug.header.stamp = rospy.Time.now()
        self.debug_pub.publish(debug)

        euler = tf.transformations.euler_from_quaternion(self.cam_pose)
        debug = PoseStamped()
        #debug.pose.orientation = Quaternion(rotq[0], rotq[1], rotq[2], rotq[3])
        debug.pose.orientation = Quaternion(euler[0], euler[1], euler[2], 0)
        debug.header.stamp = rospy.Time.now()
        self.debug_pub_2.publish(debug)
        # print(self.mav_pose)
        # print(self.cam_pose)
        # print(rotm)
        # print("rotm: ", np.shape(rotm), "k: ", np.shape(self.K))
        # print(np.shape(image))

        euler = tf.transformations.euler_from_quaternion(self.mav_pose)
        debug = PoseStamped()
        #debug.pose.orientation = Quaternion(rotq[0], rotq[1], rotq[2], rotq[3])
        debug.pose.orientation = Quaternion(euler[0], euler[1], euler[2], 0)
        debug.header.stamp = rospy.Time.now()
        self.debug_pub_3.publish(debug)

        homography = np.matmul( np.matmul(self.K, rotm), self.Kinv)#self.K * rotm * self.Kinv
        homography /= homography[2,2]
        #print(homography)
        img = cv2.warpPerspective(image, M=homography, dsize=(image.shape[1], image.shape[0]))
        img = img[50:-50,100:-100]
        #move cam pose towards mav pose for stability
        #self.cam_pose = tf.transformations.quaternion_slerp(self.cam_pose, self.mav_pose, 0.01)
        return img


    def update_model(self, imu_data):
        self.mav_pose = np.asarray([imu_data.orientation.x, imu_data.orientation.y, imu_data.orientation.z, imu_data.orientation.w])
        self.cam_pose =  tf.transformations.quaternion_multiply(self.mav_pose, self.qcam)
        # dt = self.deltaT(self.imu_stamp, imu_data.header.stamp)
        # #print(dt)
        # angVel = np.asarray([imu_data.angular_velocity.x, imu_data.angular_velocity.y, imu_data.angular_velocity.z])
        
        # theta = np.linalg.norm(angVel)*dt
        # axis = angVel/np.linalg.norm(angVel)
        # qUpdate = tf.transformations.quaternion_about_axis(theta, axis)

        #self.cam_pose = tf.transformations.quaternion_multiply(qUpdate, self.cam_pose)

        #alpha = 1 - np.square(np.inner(self.smooth_pose, self.mav_pose))
        #alpha = alpha * 0.4 + 0.2 # limit range
        
        #self.smooth_pose = tf.transformations.quaternion_slerp(self.smooth_pose, self.cam_pose, 0.05)
        
        #update stamp
        self.imu_stamp = imu_data.header.stamp
        

    def deltaT(self, T1, T2):
       return T2.secs - T1.secs + 1e-9*(T2.nsecs - T1.nsecs)




class StabilizerNode:
    def __init__(self):
        rospy.init_node("vid_stabilizer")
        

        imu_data = rospy.wait_for_message("imu/data", Imu)
        cam_info = rospy.wait_for_message("camera_info", CameraInfo)
        self.stabilizer = VideoStabilizer(imu_data, cam_info)

        self.image_pub = rospy.Publisher("stabilized_frame", Image, queue_size=10)
        self.smooth_cam_pose_pub = rospy.Publisher("stabilized_pose", PoseStamped, queue_size=10)
       
        self.imu_sub = rospy.Subscriber("imu/data", Imu, self.newGyroData)

        self.bridge = CvBridge()
        self.image_sub = rospy.Subscriber("image_raw", Image, self.newFrame)


    def newFrame(self, data):
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

        cv_image = self.stabilizer.stabilize(cv_image)
        # self.showImage(cv_image)

        try:
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
        except CvBridgeError as e:
            print(e)



    def newGyroData(self, data):
        self.stabilizer.update_model(data)
        stab_pose = PoseStamped()
        stab_pose.pose.orientation = Quaternion(self.stabilizer.smooth_pose[0], self.stabilizer.smooth_pose[1], self.stabilizer.smooth_pose[2], self.stabilizer.smooth_pose[3])
        stab_pose.header.stamp = rospy.Time.now()
        self.smooth_cam_pose_pub.publish(stab_pose)
    

def main(args):
    node = StabilizerNode()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down")
    cv2.destroyAllWindows()


if __name__ == '__main__':
    print("Launching the stabilizer")
    
    main(sys.argv)
