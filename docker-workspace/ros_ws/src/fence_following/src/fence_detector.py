#!/usr/bin/env python2

# import ROS libraries
import rospy
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Float32

import cv2 as cv
import math
import numpy as np
import tf

class FenceDetector:
    '''
        1. Use the RGB image to remove the pixels corresponding to the grass in the depth image
        2. Use the depth image to find the distance between the drone and the fence
    '''

    def __init__(self, init_node = False):
        
        # Initialize the node
        if init_node:
            rospy.init_node('fence_detector')

        # Initialize variables
        self.depthFrame = np.zeros((720, 1280), np.float32)
        self.rgbFrame = np.zeros((720, 1280, 3), np.uint8)
        self.bridge = CvBridge()

        # Setup subscribers
        ## /camera/rgb/image_raw
        rospy.Subscriber('/camera/rgb/image_raw', Image, self._rgbImageCallback)
        ## /camera/depth/image_raw
        rospy.Subscriber('/camera/depth/image_raw', Image, self._depthImageCallback)
        
        # Setup publishers
        ## /fence_detector/output_image
        self.img_pub = rospy.Publisher('/fence_detector/output_image', Image, queue_size=10)
        ## /fence_detector/distance2fence
        self.dist_pub = rospy.Publisher('/fence_detector/distance2fence', Float32, queue_size=10)

        if init_node:
            self.main()

    def _rgbImageCallback(self, data):
        try:
            self.rgbFrame = self.bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)

    def _depthImageCallback(self, data):
        try:
            self.depthFrame = self.bridge.imgmsg_to_cv2(data, "32FC1")
        except CvBridgeError as e:
            print(e)

    @staticmethod    
    def GrassDetector(frame):
        ''' Detect the grass in the frame in order to filter it from the depth image '''
        # Downscale the image to match the depth sensor resolution
        frame = cv.resize(frame, (1280, 720))

        # Convert the imput frame to the CieLAB color space
        cielab = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        
        _, filt = cv.threshold(cielab[:, :, 2], 50, 255, cv.THRESH_BINARY)

        return filt

    @staticmethod
    def NonZeroMean(img):
        ''' Compute the mean value of the non-zero pixels in the image'''
        # Get the zon zero pixels
        nonzero_coords = cv.findNonZero(img.astype(np.uint8))
        if nonzero_coords is None:
            return -1

        nonzero_coords = nonzero_coords[0]

        # Compute the mean
        avg = 0.0
        for pt in nonzero_coords:
            avg += img[pt[1], pt[0]]
        
        avg /= len(nonzero_coords)

        return avg * np.sin(0.5)

    
    def main(self):

        while not rospy.is_shutdown():
            rgbFrame = self.rgbFrame.copy()
            depthFrame = self.depthFrame.copy()

            # Detect the grass in the image
            grass = self.GrassDetector(rgbFrame)

            # Apply the mask to the depth image
            fence = depthFrame.copy()
            fence[grass == 0] = 0 

            # Create a bounding rectangle surrounding the fence            
            fence_box = cv.boundingRect(fence.astype(np.uint8))
            pt1 = fence_box[0:2]
            pt2 = (fence_box[0] + fence_box[2], fence_box[1] + fence_box[3])
            cv.rectangle(rgbFrame, pt1, pt2, (255, 0, 0))
            self.img_pub.publish(self.bridge.cv2_to_imgmsg(rgbFrame, 'bgr8'))

            # Publish the average distance in the image
            self.dist_pub.publish(self.NonZeroMean(fence))


if __name__ == "__main__":
    FD = FenceDetector(True)