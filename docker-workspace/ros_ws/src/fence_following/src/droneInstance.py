#!/usr/bin/env python2

import rospy
from sensor_msgs.msg import NavSatFix
from mavros import setpoint as SP
from geometry_msgs.msg import TwistStamped
import mavros_msgs.msg
import mavros_msgs.srv
import mavros

from eit_playground.srv import target_global_pos, target_global_posResponse, target_local_pos, target_local_posResponse

import math
import numpy as np
import tf
import utm

class Drone:
    """
        A Drone instance, used to communicate with the drone, 
        publish speed, change setpoints, etc
    """
    local_pos = [0.0] * 5
    global_pos = [0.0] * 5
    UAV_state = mavros_msgs.msg.State()

    def __init__(self):
    
        # setup subscribers
        ## /mavros/state
        rospy.Subscriber('/mavros/state', mavros_msgs.msg.State, self._state_callback)
        ## /mavros/local_position/pose
        rospy.Subscriber('/mavros/local_position/pose', SP.PoseStamped, self._local_position_callback)
        ## /mavros/global_position/global
        rospy.Subscriber('mavros/global_position/global', NavSatFix, self._global_position_callback)

        # setup the services 
        ## /mavros/cmd/arming
        self.set_arming = rospy.ServiceProxy('mavros/cmd/arming', mavros_msgs.srv.CommandBool)
        ## /mavros/set_mode
        self.set_mode = rospy.ServiceProxy('mavros/set_mode', mavros_msgs.srv.SetMode)      
        ## goto_pos_services
        self.goto_glo_pos_serv = rospy.ServiceProxy("target_global_pos", target_global_pos)
        self.goto_loc_pos_serv = rospy.ServiceProxy("target_local_pos", target_local_pos)

        # setup publisher
        ## /mavros/setpoint/position/local
        self.setpoint_local_pub = rospy.Publisher('/mavros/setpoint_position/local', SP.PoseStamped, queue_size=10)
        ## /mavros/setpoint_velocity/cmd_vel
        self.cmd_vel_pub = rospy.Publisher("/mavros/setpoint_velocity/cmd_vel", TwistStamped, queue_size=10)
        

    def _state_callback(self, topic):
        self.UAV_state.armed = topic.armed
        self.UAV_state.connected = topic.connected
        self.UAV_state.mode = topic.mode
        self.UAV_state.guided = topic.guided
    
    def _local_position_callback(self, topic):
        # Position data
        self.local_pos[0] = topic.pose.position.x
        self.local_pos[1] = topic.pose.position.y
        self.local_pos[2] = topic.pose.position.z
        
        # Orientation data
        (r, p, y) = tf.transformations.euler_from_quaternion([topic.pose.orientation.x, topic.pose.orientation.y, topic.pose.orientation.z, topic.pose.orientation.w])
        # Fix because after 180 degrees yaw becomes negative
        if y < 0.0:
            y = 2.0 * np.pi + y
        if y > 6.28:
            y = 0.0
        self.local_pos[3] = y
        self.local_pos[4] = r
        
        self.global_pos[3] = y
        self.global_pos[4] = r

    def _global_position_callback(self, data):
        self.global_pos[0] = data.latitude
        self.global_pos[1] = data.longitude
        self.global_pos[2] = data.altitude 
    
    def enableOffboard(self):
        ''' Enable the offboard mode of the drone to accept commands from the flight controller'''

        last_request = rospy.get_rostime()
        while not self.UAV_state.armed or self.UAV_state.mode != "OFFBOARD":
            now = rospy.get_rostime()

            # Set the drone to offboard mode
            if self.UAV_state.mode != "OFFBOARD":
                if(now - last_request > rospy.Duration(5.)):
                    self.set_mode(0, 'OFFBOARD')
                    rospy.loginfo("Trying to enable OFFBOARD mode")
                    last_request = now

            # Arm the drone
            if not self.UAV_state.armed:
                if(now - last_request > rospy.Duration(5.)):
                   mavros.command.arming(True)
                   rospy.loginfo("Trying to arm the drone")
                   last_request = now

            # Publish an empty message Pose to enable the offboard mode
            setpoint_msg = mavros.setpoint.PoseStamped(
            header=mavros.setpoint.Header(
                frame_id="base_footprint",
                stamp=rospy.Time.now()),
            )

            self.setpoint_local_pub.publish(setpoint_msg)
            rospy.sleep(0.1)
    
    def GotoLoc(self, pos):
        """ Go to a position in the local coordinate system"""

        loc_pos = mavros_msgs.msg.PositionTarget(
                        header=mavros.setpoint.Header(
                            frame_id="att_pose",
                            stamp=rospy.Time.now()),
                        )
        # Position
        loc_pos.position.x = pos[0]
        loc_pos.position.y = pos[1]
        loc_pos.position.z = pos[2]

        # Orientation
        loc_pos.yaw = pos[3]

        # Call the service
        try:
            dist = self.goto_loc_pos_serv(loc_pos)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
            print("Service buisy")    
    
    def GotoGlob(self, pos):
        """ Go to a position using GPS coordinates """
        glob_pos = mavros_msgs.msg.GlobalPositionTarget(
                        header=mavros.setpoint.Header(
                            frame_id="att_pose",
                            stamp=rospy.Time.now()),
                        )
            
        # Position 
        glob_pos.latitude = pos[0]
        glob_pos.longitude = pos[1]
        glob_pos.altitude = pos[2]
            
        # Orientation
        glob_pos.yaw = pos[3]
        try:
            dist = self.goto_glo_pos_serv(glob_pos)
        except rospy.ServiceException as exc:
            print("Service did not process request: " + str(exc))
            print("Service buisy")
    
    def PubSpeed(self, speed):
        """ Publish the desired speed to the drone """

        # Activate the offboard mode
        self.enableOffboard()
        
        sp = TwistStamped()
        sp.twist.linear.x = speed[0]        
        sp.twist.linear.y = speed[1]
        sp.twist.linear.z = speed[2]
        sp.twist.angular.z = speed[3]

        self.cmd_vel_pub.publish(sp)

    def Land(self):
        ''' Land the UAV at its current\given position '''
        rospy.loginfo("Drone landing")
        self.set_mode(0, 'AUTO.LAND')
        #mavros.command.arming(False)

    def Return(self):
        ''' Return to the home pos and land'''
        rospy.loginfo("Drone Returning to home")
        self.set_mode(0, 'AUTO.RTL')

    def Hover(self):
        """
            Used to keep the altitude and position of the drone stable once activated
        """
        rospy.loginfo("Drone Hovering")
        self.set_mode(0, 'AUTO.LOITER')
    
    def Glob2Loc(self, pos):
        """ 
            Convert a global position to the local coordinate system
            @pos: global position to convert
        """
        # Convert the global position to the local
        x1, y1, z1, u = utm.from_latlon(self.global_pos[0], self.global_pos[1])
        x2, y2, z2, u = utm.from_latlon(pos[0], pos[1])

        return np.array([self.local_pos[0] + x2 - x1, self.local_pos[1] +  y2 - y1])