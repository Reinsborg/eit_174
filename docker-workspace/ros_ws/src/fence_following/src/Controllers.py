#!/usr/bin/env python2

# https://github.com/ivmech/ivPID/blob/master/PID.py

import rospy
from std_msgs.msg import Float32, Bool

import time
import numpy as np
from droneInstance import Drone

class PID:
    '''
        PID controller using variable gains from rosparams
    '''

    def __init__(self, controller_type, SetPoint):

        # Get the PID parameters from the parameter server
        pid_params = rospy.get_param(controller_type)
        self.kp = pid_params['kp']
        self.ki = pid_params['ki']
        self.kd = pid_params['kd']
        self.out_h = pid_params['out_h']
        self.out_l = pid_params['out_l']
        self.sample_time = pid_params['sample_time']
        self.SetPoint = SetPoint
        
        self.current_time = time.time()
        self.last_time = self.current_time

        self.clear()

    def clear(self):
        """Clears PID computations and coefficients"""
        self.PTerm = 0.0
        self.ITerm = 0.0
        self.DTerm = 0.0
        self.last_error = 0.0
        self.int_error = 0.0
        self.output = 0.0
    
    def update(self, feedback_value):
        """Calculates PID value for given reference feedback
        .. math::
            u(t) = K_p e(t) + K_i \int_{0}^{t} e(t)dt + K_d {de}/{dt}
        .. figure:: images/pid_1.png
           :align:   center
           Test PID with Kp=1.2, Ki=1, Kd=0.001 (test_pid.py)
        """
        error = self.SetPoint - feedback_value

        self.current_time = time.time()
        delta_time = self.current_time - self.last_time
        delta_error = error - self.last_error

        if (delta_time >= self.sample_time):
            self.PTerm = self.kp * error
            self.ITerm += error * delta_time

            if (self.ITerm < -self.out_l):
                self.ITerm = -self.out_l
            elif (self.ITerm > self.out_h):
                self.ITerm = self.out_h

            self.DTerm = 0.0
            if delta_time > 0:
                self.DTerm = delta_error / delta_time

            # Remember last time and last error for next calculation
            self.last_time = self.current_time
            self.last_error = error

            self.output = self.PTerm + (self.ki * self.ITerm) + (self.kd * self.DTerm)

        return self.output

class Controllers:
    # Initialize variables
    distance2fence = 0.0
    _drone = Drone()
    stopMotion = False

    def __init__(self, distanceSetpoint = 2.0, altitudeSetpoint = 3.0, yawSetpoint = 0.0):

        self._distPID = PID('distance_controller', distanceSetpoint)
        self._altPID = PID('altitude_controller', altitudeSetpoint)
        self._yawPID = PID('yaw_controller', yawSetpoint)
        
        # Setup subscribers
        ## /fence_detector/distance2fence
        rospy.Subscriber('/fence_detector/distance2fence', Float32, self._distance2FenceCallback)
        ## /uav_pos_services/stop_motion
        rospy.Subscriber('uav_pos_services/stop_motion', Bool, self._stopMotionCallback)
    
    def _distance2FenceCallback(self, data):
        self.distance2fence = data.data

    def _stopMotionCallback(self, data):
        self.stopMotion = data.data

    def MaintainAltitude(self):
        """ Pusblish the speed to maintain the correct altitude """
        sp = [0.0] * 4
        tar_alt = self._altPID.SetPoint
        while not (tar_alt - 0.1 < self._drone.local_pos[2] < tar_alt + 0.1):
            # Check if the action was cancelled
            if self.stopMotion:
                break

            sp[2] = self._altPID.update(self._drone.local_pos[2])
            #print("Alt_PID", sp, self._drone.local_pos[2])
            self._drone.PubSpeed(sp)
            rospy.sleep(0.01)    

        return self._altPID.update(self._drone.local_pos[2])
    
    def CorrectHeading(self):
        """ Publish the speed to maintain the correct heading of the drone """
        sp = [0.0] * 4
        tar_yaw = self._yawPID.SetPoint
        while not (tar_yaw - 0.1 < self._drone.local_pos[3] < tar_yaw + 0.1):
            # Check if the action was cancelled
            if self.stopMotion:
                break
            
            # Maintain the correct altidude
            sp[2] = self.MaintainAltitude()

            sp[3] = self._yawPID.update(self._drone.local_pos[3])
            #print(tar_yaw, self._drone.local_pos[3], sp[3])
            #print("Alt_PID", sp, self._drone.local_pos[2])
            self._drone.PubSpeed(sp)
            rospy.sleep(0.01)    

        return self._yawPID.update(self._drone.local_pos[3])


    def AlignWithFence(self):
        """ 
            Use the distPID controller to align the drone with the fence 
        """
        if not (1.0 < self.distance2fence < 6.0):
            return None

        sp = [0.0] * 4
        while not (1.9 < self.distance2fence < 2.1):
            # Check if the action was cancelled
            if self.stopMotion:
                break
            
            # Maintain the correct altidude
            sp[2] = self.MaintainAltitude()

            # Maintain the correct orientation facing the fence
            #sp[3] = self.CorrectHeading()

            # Maintain the distance to the fence
            ang = self._drone.local_pos[3]
            spx = self._distPID.update(self.distance2fence)
            #print(ang, spx)
            sp[0] = -spx * np.cos(ang)
            sp[1] = -spx * np.sin(ang)
            '''
            if -1.57 < ang < 1.57:
                sp[0] = -spx * np.sign(np.cos(ang))
            else:
                sp[1] = -spx * np.sign(np.sin(ang))

            
            #sp[0] = np.sin(np.pi + ang) * spy
            #sp[1] = np.cos(np.pi + ang) * spy
            if abs(sp[0]) > abs(sp[1]):
                #sp[0] = np.sign(np.sin(np.pi + ang)) * spy
                sp[0] = np.sign(np.sin(ang)) * spy
                sp[1] = 0.0
            else:
                sp[0] = 0.0
                #sp[1] = np.sign(np.cos(np.pi + ang)) * spy
                sp[1] = np.sign(np.cos(ang)) * spy
            '''
            #print("Dist_PID", sp, self.distance2fence)
            self._drone.PubSpeed(sp)
            rospy.sleep(0.01)    

        return self._distPID.update(self.distance2fence)
    