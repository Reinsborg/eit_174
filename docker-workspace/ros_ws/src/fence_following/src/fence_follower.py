#!/usr/bin/env python2

# import ROS libraries
import rospy
from std_msgs.msg import Float32, Bool
import actionlib

import math
import numpy as np
import tf
import threading

import fence_following.msg as ffm
from droneInstance import Drone
from Controllers import Controllers

from dynamic_reconfigure.server import Server
from fence_following.cfg import pid_dynamic_paramsConfig

class FollowFence:
    # Create messages that are used to publish feedback/result
    _result = ffm.FenceFollowerMsgResult()
    distance2fence = 0.0
    _drone = Drone()
    _controllers = Controllers()

    def __init__(self):

        # Init node
        rospy.init_node('fence_follower')

        # Setup Publishers
        self.stopMotion_pub = rospy.Publisher('uav_pos_services/stop_motion', Bool, queue_size=10)

        # Setup subscribers
        ## /fence_detector/distance2fence
        rospy.Subscriber('/fence_detector/distance2fence', Float32, self._distance2FenceCallback)

        # Setup the action server
        self._as = actionlib.SimpleActionServer('follow_fence', ffm.FenceFollowerMsgAction, self._actionCallback, auto_start = False)
        self._as.start()

        rospy.loginfo("Fence following action servicer initiated successfully")
        # Setup rate
        rospy.spin()

    def _configCallback(self, config, level):
        self._distPID.kp = config['kp']
        self._distPID.ki = config['ki']
        self._distPID.kd = config['kd']
        self._distPID.out_h = config['out_h']
        self._distPID.out_l = config['out_l']
        self._distPID.sample_time = config['sample_time']

        return config

    def _distance2FenceCallback(self, data):
        self.distance2fence = data.data
    
    def Status(self, stPos, tarPos): 
        """ Compute the precentage of the path already covered """
        # Compute the distance between the start and the final position
        tot_dist = np.linalg.norm(np.array(stPos) - np.array(tarPos))

        # Compute the distance between the current position and the starting position
        cur_dist = np.linalg.norm(np.array(self._drone.local_pos[0:2]) - np.array(stPos))

        # Find the percentage of the total distance covered
        status = cur_dist / tot_dist
        
        #print(status, cur_dist, tot_dist)
        
        # Publish the feedback on the action service
        feedback = ffm.FenceFollowerMsgFeedback()
        feedback.status.data = status
        self._as.publish_feedback(feedback)

        return status

    def GotoInPos(self, inPos):
        """ Goto the starting position """
        # Use a thread so that it can be cancelled
        thr = threading.Thread(target=self._drone.GotoGlob, args=(inPos,))
        thr.start()
        cancelled = False
        
        # Wait until the thread finished or the mission gets cancelled
        while not cancelled and thr.is_alive():
            rospy.sleep(.1)
            if self._as.is_preempt_requested():
                rospy.loginfo("Goal was cancelled")
                cancelled = True
                self.stopMotion_pub.publish(True)

        # Delete the thread
        del(thr)
        
        return cancelled

    def _actionCallback(self, goal):
        
        # Setup rate
        rospy.Rate(10)
        orient = goal.orient.data
        inPos = [float("{:.7f}".format(goal.in_pos[0].data)), float("{:.7f}".format(goal.in_pos[1].data)), goal.in_pos[2].data, orient]
        finPos = [float("{:.7f}".format(goal.fin_pos[0].data)), float("{:.7f}".format(goal.fin_pos[1].data)), goal.fin_pos[2].data, orient]
        max_spy = goal.speed.data
        spy = 0.1
        alignmentMethod = goal.alignmentMethod.data
        succeeded = True
        
        # Convert the Global start and goal positions to local
        inPos_loc = self._drone.Glob2Loc(inPos)
        finPos_loc = self._drone.Glob2Loc(finPos)

        # Start the PID controllers
        self._controllers._distPID.clear()
        self._controllers._altPID.clear()
        self._controllers._yawPID.clear()
        self._controllers._yawPID.SetPoint = inPos[3]
        self._controllers._altPID.SetPoint = inPos[2] 

        # Print the targets
        rospy.loginfo("Initial pos {}".format(inPos))
        rospy.loginfo("Final pos {}".format(finPos))

        # Go to the initial position
        cancelled = self.GotoInPos(inPos)
        # Execute it twice to make it more accurate on long distances
        if not cancelled:
            cancelled = self.GotoInPos(inPos)
        
        if not cancelled:
            rospy.loginfo("Drone reached first waypoint")

            # Start the fence following
            while self.Status(inPos_loc, finPos_loc) < 0.99:
            #while not rospy.is_shutdown():
            #for i in range(10):
                # Check if the goal is cancelled
                if self._as.is_preempt_requested():
                    rospy.loginfo("Goal was cancelled")
                    succeeded = False
                    canceled = True
                    #self._as.set_preempted()
                    break

                # Align the drone with the fence
                sp = [0.0] * 4
                #sp[1] = -np.abs(spy)
                beg = rospy.Time.now()
                sp[2] = self._controllers.MaintainAltitude()
                #sp[3] = self._controllers.CorrectHeading()
                spx = self._controllers.AlignWithFence()
                
                if rospy.Time.now() - beg > rospy.Duration(0.1):
                    spy = 0.1
                else:
                    spy += 0.001 * max_spy
                    if spy > max_spy:
                        spy = max_spy
                #spy = self._controllers.SmoothRollSpeed(max_spy)
                #print(spy)
                if not spx:
                    spx = 0.0
                
                # Fix the speed depending on the orietation of the drone
                ang = self._drone.local_pos[3]
                '''
                if -1.57 < ang < 1.57:
                    sp[0] = spx * np.sign(np.cos(ang))
                    sp[1] = -np.abs(spy)# * np.sign(np.sin(ang))
                else:
                    sp[1] = spx * np.sign(np.sin(ang))
                    sp[0] = -np.abs(spy)# * np.sign(np.cos(ang))
                #sp[0] = np.cos(np.pi + ang) * spy - np.sin(np.pi + ang) * spx 
                #sp[1] = np.sin(np.pi + ang) * spy + np.cos(np.pi + ang) * spx
                #print(ang, sp[0], sp[1])
                '''
                sp[0] = -spx * np.cos(ang) + np.abs(spy) * np.sin(ang)
                sp[1] = -spx * np.sin(ang) - np.abs(spy) * np.cos(ang)

                # Publish the speed to the drone
                self._drone.PubSpeed(sp)                
                rospy.sleep(0.01)
        else:
            succeeded = False    

        # Generate the feedback result message
        self._result.pos[0].data = self._drone.global_pos[0]
        self._result.pos[1].data = self._drone.global_pos[1]
        self._result.pos[2].data = self._drone.global_pos[2]
        self._result.pos[3].data = self._drone.global_pos[3]

        rospy.sleep(.1)
        if succeeded:
            rospy.loginfo("Given fence section successfully followed")
            #self._drone.Land()
            self._as.set_succeeded(self._result)
        else:
            self._as.set_aborted(self._result)
            self.stopMotion_pub.publish(False)
        

if __name__ == "__main__":
    FollowFence()