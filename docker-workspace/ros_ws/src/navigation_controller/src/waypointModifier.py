#!/usr/bin/env python2

'''
    Read the waypoints sent from QGroundControl
    Add more waypoints around the corners to make the route smoother
'''
# import ROS libraries
import rospy
import mavros
from mavros_msgs.msg import Waypoint as WP
from mavros_msgs.msg import WaypointList as WPL
from mavros_msgs.srv import WaypointClear, WaypointPush
import numpy as np
import math
from copy import deepcopy as dp

class WaypointModifier:

    def __init__(self, start_node = False):

        if start_node:
            rospy.init_node('waypoint_modifier')

        # Initialize variables
        self.Waypoints_in = []
        self.WaypointList_in = []
        self.Waypoints_out = []
        self.WaypointList_out = []

        # Setyp namespace
        mavros.set_namespace('mavros')

        # Setup subscribers
        # /mavros/mission/waypoints
        rospy.Subscriber('mavros/mission/waypoints', WPL, self._waypointCallback)

        # Setup publishers
        # /mavros/mission/waypoints
        self.wpl_pub = rospy.Publisher('mavros/mission/waypoints', WPL, queue_size=10)

        # Setup services
        self.wp_clear = rospy.ServiceProxy('/mavros/mission/clear', WaypointClear)
        self.wp_push = rospy.ServiceProxy('/mavros/mission/push', WaypointPush)
        
    def _waypointCallback(self, data):
        ''' Read the waypoints and store them in a list ''' 
        
        rospy.loginfo("New mission detected")

        wps = data.waypoints
        speed = 5.0
        
        # clear the previous list
        self.Waypoints_in = []
        self.WaypointList_in = []

        # Read and store the waypoints
        for i in range(len(wps)):
            self.WaypointList_in.append(wps[i])
            lat = wps[i].x_lat
            lon = wps[i].y_long
            alt = wps[i].z_alt
            if math.isnan(wps[i].param4):
                heading = 0.0
            else:
                heading = wps[i].param4
            hold = wps[i].param1
            tmp = []

            # Store the waypoints in a more helpful list
            if wps[i].command == 16 or wps[i].command == 22 or wps[i].command == 21: # waypoint or takeoff
                tmp = [lat, lon, alt, speed, heading, hold]
            elif wps[i].command == 530:
                pass
            elif wps[i].command == 178:  # change speed 
                self.Waypoints_in[-1][3] = wps[i].param2
                speed = wps[i].param2
            elif wps[i].command == 20:
                tmp = self.Waypoints_in[0]
                tmp[-1] = 0.0
            else:
                rospy.logwarn('Waypoint command not recognized' + str(wps[i].command))

            if len(tmp) > 0:
                self.Waypoints_in.append(tmp)

        self.WaypointList_in = np.array(self.WaypointList_in)
        self.Waypoints_in = np.array(self.Waypoints_in)

    def ClearMission(self):
        self.wp_clear()
    
    def PushMission(self, waypoints = []):
        if len(waypoints) == 0:
            self.wp_push(start_index = 0, waypoints = self.WaypointList_out)
        else:
            self.wp_push(start_index = 0, waypoints = waypoints)

    def GetWaypoints(self):
        return self.Waypoints_in, self.Waypoints_out

    def GetWaypointList(self):
        return self.WaypointList_in, self.WaypointList_out
    
    def AddWaypoints(self):
        ''' Add more waypoints to make the path planning more accurate '''
        # Make a copy of the waypoint list for processing
        wpl = self.WaypointList_in.copy()
        self.WaypointList_out = []
        self.Waypoints_out = []
        error = 0

        cnt = 0
        if len(wpl) == 0:
            rospy.logwarn('No mission plan detected')
            return 1
        
        if abs(wpl[0].x_lat) > 180:
            self.WaypointList_out.append(wpl[cnt])
            cnt += 1

        while wpl[cnt].command != 21:
            
            if len(wpl) == cnt  + 1:
                if wpl[cnt].command != 21:
                    error = 2
                    break
            
            # Add the current item
            self.WaypointList_out.append(wpl[cnt])
            
            # Don't add extra waypoints from the takeoff point to the second point
            # Don't add extra waypoints from the last point until the landing
            if wpl[cnt].command != 22 and wpl[cnt + 1].command != 21:
                in_index = cnt
                x1 = wpl[cnt].x_lat
                y1 = wpl[cnt].y_long
                z = wpl[cnt].z_alt
                speed = 2.0

                # Check if the speed changes on this waypoint
                if wpl[cnt + 1].command == 178:
                    cnt += 1
                    self.WaypointList_out.append(wpl[cnt])
                    speed = wpl[cnt].param2

                # Get the next waypoint
                x2 = wpl[cnt + 1].x_lat
                y2 = wpl[cnt + 1].y_long

                # Add waypoints along the way
                wpk = dp(wpl[in_index])
                if math.isnan(wpl[in_index + 1].param4):
                    heading = wpk.param4 = 0.0
                else:
                    heading = wpk.param4 = wpl[in_index + 1].param4
            

                # lat, lon, alt, speed, heading, hold_time
                tmp = [x1, y1, z, speed, heading, 0.0]
                self.Waypoints_out.append(tmp)

                for k in [0.1, 0.25, 0.5, 0.75, 0.9]:
                    x_k = x1 + k * (x2 - x1)
                    y_k = y1 + k * (y2 - y1)

                    # If the first command is take of convert the next ones to waypoints
                    if wpk.command == 22:
                        wpk.command = 16
                        wpk.param1 = 0.0

                    wpk.x_lat = x_k
                    wpk.y_long = y_k
                    tmp = [x_k, y_k, z, speed, heading, 0.0]
                    self.WaypointList_out.append(dp(wpk))
                    self.Waypoints_out.append(tmp)

            cnt += 1

        # Add the final waypoint - Land the drone
        self.WaypointList_out.append(wpl[-1])
        
        return error

###################################################################################################
if __name__ == "__main__":
    WPM = WaypointModifier(True)

    # Print the initial waypoints    
    #print(WPM.GetWaypoints())
    #print("------------------------")

    # Add more waypoints along the path
    error = WPM.AddWaypoints()
    WPM.ClearMission()
    WPM.PushMission()
    #rospy.sleep(5)

    # Print the new waypoints
    #print(WPM.GetWaypoints())
