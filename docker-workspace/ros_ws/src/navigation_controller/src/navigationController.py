#!/usr/bin/env python2

# import ROS libraries
import rospy
import actionlib
from std_msgs.msg import Float64, Bool

import math
import numpy as np
import tf
import threading
from copy import deepcopy as dp

import navigation_controller.msg as NCM
import fence_following.msg as FFM
import droneInstance
import waypointModifier as WPM

class NavigationController:
    """
        Class that handles the autonomous navigation of the drone around the perimeter fence
    """
    _wpm = WPM.WaypointModifier()
    errors = {1:" No mission plan detected", 2:" Forgot landing command"}
    fff_status = 0.0
    _drone = droneInstance.Drone()

    def __init__(self):

        # Start the node
        rospy.init_node('navigation_controller')

        # Setup Publishers
        self.stopMotion_pub = rospy.Publisher('uav_pos_services/stop_motion', Bool, queue_size=10)

        # Setup subscribers
        # /follow_fence/feedback
        rospy.Subscriber('/follow_fence/feedback', FFM.FenceFollowerMsgActionFeedback, self._fffCallback)
        
        # Setup the action server
        self._as = actionlib.SimpleActionServer('navigation_controller/inspect_fence', NCM.FenceInspectionAction, self._actionCallback, auto_start = False)
        self._as.start()

        # Setup the action client
        self._followFenceClient = actionlib.SimpleActionClient('follow_fence', FFM.FenceFollowerMsgAction)
        self._followFenceClient.wait_for_server()

        # Keep the action server active
        rospy.loginfo("Navigation controller action servicer initiated successfully")
        rospy.spin()
    
    def _fffCallback(self, data):
        self.fff_status = data.feedback.status.data

    def AddWaypoints(self):
        """
            Add more waypoints to make it easier/ more precise for the uav to follow 
        """
        # Add the waypoints in the mission
        error = self._wpm.AddWaypoints()

        if error == 0:
            # Clear the previous mission
            self._wpm.ClearMission()

            # Upload the new mission
            self._wpm.PushMission()

        return error
    
    def Status(self, inPos, finPos, orient, speed, alignmentMethod):
        """ Publish the status of the action being executed """
        # Generate the feedback message
        feedback = NCM.FenceInspectionFeedback()
        feedback = self.CopyGoals(inPos, finPos, feedback)
        #feedback.in_pos = inPos
        #feedback.fin_pos = finPos
        feedback.orient.data = orient
        feedback.speed.data = speed
        feedback.alignmentMethod.data = alignmentMethod
        feedback.status.data = self.fff_status

        # Publish the feedback message
        self._as.publish_feedback(feedback)

        return self.fff_status
    
    @staticmethod
    def CopyGoals(inPos, finPos, goal):
        ''' Copy the inPos and finPos to the goal message for the action service'''
        # Initial position
        goal.in_pos[0].data = inPos[0]
        goal.in_pos[1].data = inPos[1] 
        goal.in_pos[2].data = inPos[2]

        # Final position
        goal.fin_pos[0].data = finPos[0]
        goal.fin_pos[1].data = finPos[1] 
        goal.fin_pos[2].data = finPos[2]

        return goal  

    def _actionCallback(self, goal):

        # Setup rate
        rospy.Rate(10)

        # Store the action call input data
        addWaypoints = goal.addWaypoints.data
        startInspection = goal.startInspection.data
        alignmentMethod = goal.alignmentMethod.data
        error = 0
        
        # Status variables
        succeeded = True
        canceled = False

        # Add more waypoints to the current track
        if addWaypoints:
            rospy.loginfo("Adding more waypoints")
            error = self.AddWaypoints()
        
        if error > 0:
            rospy.logerr("An error occured" + self.errors[error])
            canceled = True
            succeeded = False
            startInspection = False

        # Start processing the waypoints
        if len(self._wpm.Waypoints_out) == 0:
            wps = dp(self._wpm.Waypoints_in)
        else:    
            wps = dp(self._wpm.Waypoints_out)
        
        if len(wps) == 0:
            rospy.logwarn("Mission not detected")
            succeeded = False

        # Begin the inspection
        if startInspection:
            rospy.loginfo("Starting the inspection")

            for cnt in range(1, len(wps) - 2):
                #print(cnt, wps[cnt][0:4], wps[cnt + 1][0:4])                
                # Generate goal for the fence following action
                # lat, lon, alt, speed, heading, hold_time
                goal = FFM.FenceFollowerMsgGoal()
                inPos = [wps[cnt][0], wps[cnt][1], wps[cnt][2]]
                finPos = [wps[cnt + 1][0], wps[cnt + 1][1], wps[cnt + 1][2]]
                goal = self.CopyGoals(inPos, finPos, goal)
                #goal.in_pos = inPos
                #goal.fin_pos = finPos
                goal.orient.data = -wps[cnt][4] * np.pi / 180.0
                # Fix because after 180 degrees yaw becomes negative
                if goal.orient.data < 0.0:
                    goal.orient.data = 2.0 * np.pi + goal.orient.data
                goal.speed.data = wps[cnt][3]
                goal.alignmentMethod.data = alignmentMethod

                # Send the goal
                self._followFenceClient.send_goal(goal)

                # Wait for the action to finish or cancelled
                while self.Status(inPos, finPos, wps[cnt][4], wps[cnt][3], alignmentMethod) < 0.99:
                    # Check if the goal is cancelled
                    if self._as.is_preempt_requested():
                        rospy.loginfo("Goal was cancelled")
                        self.stopMotion_pub.publish(True)

                        # Cancel the fence following action
                        self._followFenceClient.cancel_goal()

                        succeeded = False
                        canceled = True
                        #self._as.set_preempted()
                        rospy.sleep(.1)
                        break
                
                if canceled:
                    break
                
                self.fff_status = 0.0

        rospy.sleep(.1)
        result =  NCM.FenceInspectionResult()
        if succeeded:
            rospy.loginfo("Inspection finished successfully")
            if startInspection:
                # Go to the landing position and land
                pos = [wps[-1][0], wps[-1][1], 3.0, 0.0]
                rospy.loginfo("Going to landing position{}".format(pos[0:2]))
                self._drone.GotoGlob(pos)
                self._drone.Land()
            self._as.set_succeeded(result)
        else:
            self._drone.Hover()
            self.stopMotion_pub.publish(False)
            self._as.set_aborted(result)
###################################################################################################
if __name__ == "__main__":
    NavigationController()
